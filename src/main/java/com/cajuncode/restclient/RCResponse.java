package com.cajuncode.restclient;

/**
 * Created by kevinb on 10/9/13.
 */
public class RCResponse {

    private int status_code;

    private String content;

    public RCResponse(int status_code, String content) {
        this.status_code = status_code;
        this.content = content;
    }

    public int getStatus_code() {
        return status_code;
    }

    public void setStatus_code(int status_code) {
        this.status_code = status_code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
