package com.cajuncode.restclient;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.URI;
        import java.net.URISyntaxException;
        import java.util.ArrayList;
        import java.util.List;
        import java.util.Map;

        import org.apache.http.HttpResponse;
        import org.apache.http.NameValuePair;
        import org.apache.http.client.ClientProtocolException;
        import org.apache.http.client.HttpClient;
        import org.apache.http.client.entity.UrlEncodedFormEntity;
        import org.apache.http.client.methods.HttpDelete;
        import org.apache.http.client.methods.HttpGet;
        import org.apache.http.client.methods.HttpPost;
        import org.apache.http.client.methods.HttpPut;
        import org.apache.http.client.methods.HttpRequestBase;
        import org.apache.http.client.utils.URIUtils;
        import org.apache.http.client.utils.URLEncodedUtils;
        import org.apache.http.conn.ClientConnectionManager;
        import org.apache.http.conn.scheme.Scheme;
        import org.apache.http.conn.scheme.SchemeRegistry;
        import org.apache.http.conn.ssl.SSLSocketFactory;
        import org.apache.http.conn.ssl.X509HostnameVerifier;
        import org.apache.http.entity.ContentType;
        import org.apache.http.entity.mime.MultipartEntityBuilder;
        import org.apache.http.impl.client.DefaultHttpClient;
        import org.apache.http.message.BasicNameValuePair;

        import android.os.AsyncTask;
        import android.util.Log;

        import javax.net.ssl.SSLContext;
        import javax.net.ssl.SSLException;
        import javax.net.ssl.SSLSession;
        import javax.net.ssl.SSLSocket;
        import javax.net.ssl.TrustManager;
        import javax.net.ssl.X509TrustManager;
        import javax.security.cert.CertificateException;
        import javax.security.cert.X509Certificate;

public class RestClient {
    private static final String TAG = RestClient.class.getSimpleName();
    private static RestClient instance;

    private Map<String, String> headers;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    private void setHeadersToRequest(HttpRequestBase request){
        if(headers != null && !headers.isEmpty()){
            for(String key: headers.keySet())
                request.setHeader(key,  headers.get(key));

        }
    }

    private Map<String, RCAttachement> attachments;

    public Map<String, RCAttachement> getAttachments() {
        return attachments;
    }

    public void setAttachements(Map<String, RCAttachement> attachments) {
        this.attachments = attachments;
    }

    private void addAtachementsToBuilder(MultipartEntityBuilder builder){
        if(attachments != null && !attachments.isEmpty()){
            for(String key: attachments.keySet()) {
                Log.d(TAG, attachments.get(key).getName() + " size is " + attachments.get(key).getContent().length);
                builder.addBinaryBody(key, attachments.get(key).getContent(), ContentType.DEFAULT_BINARY, attachments.get(key).getName());
            }
        }
    }

    private String host;
    private int port;
    private String schema;
    public RestClient(String schema, String host, int port){
        this.host = host;
        this.port = port;
        this.schema = schema;

        if (instance == null)
            instance = this;
    }

    public static String baseURL(){
        StringBuilder b = new StringBuilder();
        RestClient rc  = RestClient.sharedClient();
        b.append(rc.schema);
        b.append("://");
        b.append(rc.host);
        if (rc.port != -1){
            b.append(":");
            b.append(rc.port);
        }
        return b.toString();

    }

    public static RestClient sharedClient(){
        return instance;
    }
    public static String processResponseToString(InputStream inputStream){
        String result = null;
        try {
            BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
            StringBuilder sBuilder = new StringBuilder();

            String line = null;
            while ((line = bReader.readLine()) != null) {
                sBuilder.append(line + "\n");
            }

            inputStream.close();
            result = sBuilder.toString();

        } catch (Exception e) {
            Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
        }
        return result;
    }

    public RCResponse get(final String path,final Map<String, String> queryParams) {
        HttpResponse response = null;
        RCResponse rc_response = null;
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        for (String key: queryParams.keySet()){
            qparams.add(new BasicNameValuePair(key, queryParams.get(key)));
        }
        URI uri;
        try {
            uri = URIUtils.createURI(this.schema, this.host, this.port, path,
                    URLEncodedUtils.format(qparams, "UTF-8"), null);

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet get = new HttpGet(uri);
            setHeadersToRequest(get);
//            for (String key: queryParams.keySet()){
//                get.addHeader(key, queryParams.get(key));
//            }

            response =  httpclient.execute(get);
            int status_code = response.getStatusLine().getStatusCode();
            String content = this.processResponseToString(response.getEntity().getContent());
            rc_response = new RCResponse(status_code, content);

        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rc_response;
    }

    public void get(final String path, final Map<String, String> queryParams, final RestResponseListener listener){
        GetAsyncTask pat = new GetAsyncTask();
        pat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RCRequest(path, queryParams, listener));
    }

    /**
     * Post
     * @param path
     * @param params
     * @return
     */
    public RCResponse post(final String path, final Map<String, String>params){
        HttpResponse response = null;
        HttpClient httpclient = new DefaultHttpClient();
        RCResponse rc_response = null;
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        try {
            URI uri = URIUtils.createURI(this.schema, this.host, this.port, path,
                    null, null);
            HttpPost httppost = new HttpPost(uri);
            // Add your data
            for(String key: params.keySet()){
                builder.addTextBody(key, params.get(key));
            }
            setHeadersToRequest(httppost);
            addAtachementsToBuilder(builder);
            httppost.setEntity(builder.build());

            // Execute HTTP Post Request
            response = httpclient.execute(httppost);

            int status_code = response.getStatusLine().getStatusCode();
            Log.e("com.cajuncode.RestClient", "Response Status Code: " + status_code);
            String content = this.processResponseToString(response.getEntity().getContent());
            Log.e("com.cajuncode.RestClient", "Response: " + content);
            rc_response = new RCResponse(status_code, content);
            this.attachments = null;

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rc_response;
    }

    /**
     * Post Async
     * @param path
     * @param params
     * @param listener
     */
    public void post(final String path, final Map<String, String>params, final RestResponseListener listener){
        PostAsyncTask pat = new PostAsyncTask();
        pat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RCRequest(path, params, listener));

    }

    public RCResponse put(final String path, final Map<String, String> params){
        HttpResponse response = null;
        HttpClient httpclient = new DefaultHttpClient();
        RCResponse rc_response = null;
        try {
            URI uri = URIUtils.createURI(this.schema, this.host, this.port, path,
                    null, null);
            HttpPut httpput = new HttpPut(uri);
            setHeadersToRequest(httpput);
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(params.size());
            for(String key: params.keySet()){
                nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
            }
            httpput.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            response = httpclient.execute(httpput);

            int status_code = response.getStatusLine().getStatusCode();
            String content = this.processResponseToString(response.getEntity().getContent());
            rc_response = new RCResponse(status_code, content);


        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rc_response;
    }

    public void put(final String path, final Map<String, String>params, final RestResponseListener listener){
        PutAsyncTask pat = new PutAsyncTask();
        pat.execute(new RCRequest(path, params, listener));

    }

    public RCResponse delete(final String path){
        HttpResponse response = null;
        HttpClient httpclient = new DefaultHttpClient();
        RCResponse rc_response = null;
        try {
            URI uri = URIUtils.createURI(this.schema, this.host, this.port, path,
                    null, null);
            HttpDelete httpdelete = new HttpDelete(uri);
            setHeadersToRequest(httpdelete);
            // Execute HTTP Post Request
            response = httpclient.execute(httpdelete);

            int status_code = response.getStatusLine().getStatusCode();
            String content = this.processResponseToString(response.getEntity().getContent());
            rc_response = new RCResponse(status_code, content);


        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rc_response;
    }

    public void delete(final String path, final RestResponseListener listener){
        DeleteAsyncTask pat = new DeleteAsyncTask();
        pat.execute(new RCRequest(path, null, listener));

    }

//    public static HttpClient wrapClient(HttpClient base) {
//        try {
//            SSLContext ctx = SSLContext.getInstance("TLS");
//            X509TrustManager tm = new X509TrustManager() {
//
//                public void checkClientTrusted(X509Certificate[] xcs, String string) throws CertificateException {
//                }
//
//                public void checkServerTrusted(X509Certificate[] xcs, String string) throws CertificateException {
//                }
//
//                public X509Certificate[] getAcceptedIssuers() {
//                    return null;
//                }
//            };
//            X509HostnameVerifier verifier = new X509HostnameVerifier() {
//
//                @Override
//                public void verify(String string, SSLSocket ssls) throws IOException {
//                }
//
//                @Override
//                public void verify(String string, X509Certificate xc) throws SSLException {
//                }
//
//                @Override
//                public void verify(String string, String[] strings, String[] strings1) throws SSLException {
//                }
//
//                @Override
//                public boolean verify(String string, SSLSession ssls) {
//                    return true;
//                }
//            };
//            ctx.init(null, new TrustManager[]{tm}, null);
//            SSLSocketFactory ssf = new SSLSocketFactory(ctx);
//            //ssf.setHostnameVerifier(verifier);
//            ClientConnectionManager ccm = base.getConnectionManager();
//            SchemeRegistry sr = ccm.getSchemeRegistry();
//            sr.register(new Scheme("https", ssf, 443));
//            return new DefaultHttpClient(ccm, base.getParams());
//        } catch (Exception ex) {
//            ex.printStackTrace();
//            return null;
//        }
//    }

    private class RCRequest{
        public String path;
        public  Map<String, String>params;
        public  RestResponseListener listener;
        public RCRequest(String path, Map<String, String> params,
                         RestResponseListener listener) {
            super();
            this.path = path;
            this.params = params;
            this.listener = listener;
        }

    }

    private class PostAsyncTask extends AsyncTask<RCRequest, Integer , RCResponse>{
        private RCRequest request;
        @Override
        protected RCResponse doInBackground(RCRequest... requests) {
            request = requests[0];
            return post(requests[0].path, requests[0].params);

        }
        @Override
        protected void onPostExecute(RCResponse result) {
            request.listener.processResponse(result);
            super.onPostExecute(result);
        }

    }

    private class PutAsyncTask extends AsyncTask<RCRequest, Integer , RCResponse>{
        private RCRequest request;
        @Override
        protected RCResponse doInBackground(RCRequest... requests) {
            request = requests[0];
            return put(requests[0].path, requests[0].params);

        }
        @Override
        protected void onPostExecute(RCResponse result) {
            request.listener.processResponse(result);
            super.onPostExecute(result);
        }

    }

    private class DeleteAsyncTask extends AsyncTask<RCRequest, Integer , RCResponse>{
        private RCRequest request;
        @Override
        protected RCResponse doInBackground(RCRequest... requests) {
            request = requests[0];
            return delete(requests[0].path);

        }
        @Override
        protected void onPostExecute(RCResponse result) {
            request.listener.processResponse(result);
            super.onPostExecute(result);
        }

    }

    private class GetAsyncTask extends AsyncTask<RCRequest, Integer , RCResponse>{
        private RCRequest request;
        @Override
        protected RCResponse doInBackground(RCRequest... requests) {
            request = requests[0];
            return RestClient.this.get(requests[0].path, requests[0].params);
        }
        @Override
        protected void onPostExecute(RCResponse result) {
            request.listener.processResponse(result);
            super.onPostExecute(result);
        }
    }
}