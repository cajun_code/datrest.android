package com.cajuncode.restclient;

import org.apache.http.HttpResponse;

/**
 * Created by alley on 10/2/13.
 */
public interface RestResponseListener {
    public void processResponse(RCResponse response);
}
