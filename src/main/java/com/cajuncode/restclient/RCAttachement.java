package com.cajuncode.restclient;

/**
 * Created by alley on 11/27/13.
 */
public class RCAttachement {

    private String name;
    private byte[] content;

    public RCAttachement(String name, byte[] content) {
        this.name = name;
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
