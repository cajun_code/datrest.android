package com.cajuncode.datrest.models;

import android.content.Intent;
import android.util.Log;

import com.cajuncode.datrest.util.Strings;
import com.cajuncode.restclient.RCResponse;
import com.cajuncode.restclient.RestClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.cajuncode.restclient.RestResponseListener;
import com.googlecode.openbeans.BeanInfo;
import com.googlecode.openbeans.IntrospectionException;
import com.googlecode.openbeans.Introspector;
import com.googlecode.openbeans.PropertyDescriptor;

/**
 * Created by alley on 10/4/13.
 */
public abstract class DataObject implements Serializable {
    private static final String TAG = DataObject.class.getSimpleName().toLowerCase();

    public abstract String getURL();

    public String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public ArrayList fromJson(String json){
        ArrayList data = null;
        Object thisJSON = null;
        try {
            thisJSON = new JSONTokener(json).nextValue();
            if (thisJSON instanceof JSONObject) {
                //you have an object
                Log.e("****Process: ", "Object");

                JSONObject object = new JSONObject(json);
                if (object.has(getPurialMetaKey())) {
                    data = fromJson(object.getJSONArray(getPurialMetaKey()));
                }else if (object.has(getMetaKey())) {
                    processObject(object.getJSONObject(getMetaKey()), this);
                }else {
                    processObject(object, this);
                }
            } else if (thisJSON instanceof JSONArray) {
                //you have an array
                Log.e("****Process: ", "Array");

                JSONArray object = new JSONArray(json);
                data = fromJson(object);
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
//        try{
//            if(json.charAt(0)== '['){
////                Log.e("****Process: ", "Array");
//                JSONArray array = new JSONArray(json);
//                data = fromJson(array);
//            }else{
////                Log.e("****Process: ", "Object");
//                JSONObject object = new JSONObject(json);
//                if(object.has("objects")) {
//                    data = fromJson(object.getJSONArray("objects"));
//                } else {
////                    Log.e("****Process Object", "Length: " + object.length() + " Object: " + object.toString());
//                    processObject(object, this);
//                }
//            }
//        }catch(JSONException e){}
        return data;
    }

    private ArrayList fromJson(JSONArray array){
        return fromJson(array, this);
    }

    private ArrayList fromJson(JSONArray array, Object me){
//        Log.e("****Process Array", "Length: " + array.length() + " Array: " + array.toString());
        ArrayList list = new ArrayList();
        try{
            for(int i = 0; i < array.length(); i++){
                JSONObject object = array.getJSONObject(i);
//                Log.e("****Process Object", "Length: " + object.length() + " Object: " + object.toString());

                // Create Post object here
                Object data = ((Object)me).getClass().newInstance();
                processObject( object, data);
                list.add(data);
            }
        }catch(JSONException e){} catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return list;
    }

    private void processObject(JSONObject obj, Object me){
        try{
            synchronized (this){
                BeanInfo info = Introspector.getBeanInfo(me.getClass());
                Map propertyMap = new HashMap();
                for(PropertyDescriptor d:info.getPropertyDescriptors()){
                    propertyMap.put(d.getName(), d);
                }
                Iterator iterator = obj.keys();
                while( iterator.hasNext()){
                    String key = (String)iterator.next();
                    if(propertyMap.containsKey(key)){
                        PropertyDescriptor descriptor = (PropertyDescriptor) propertyMap.get(key);
                        Class type = descriptor.getPropertyType();
                        Method m = descriptor.getWriteMethod();
                        Log.d("****Process Object", "Key: " + key);
                        Log.e("****Process Object", "Field: " + descriptor);
                        Log.e("****Process Object", "Type: " + type);
                        Log.d("****Process Object", "Value: " + obj.get(key));


                        if (!obj.isNull(key)){
                            if(type.getName().contains("String")){
                                m.invoke(me, obj.getString(key));
                            }else if(type.getName().contains("Date")){
                                m.invoke(me,new Date(obj.getLong(key)*1000));
                            }else if(type.getName().contains("Boolean")){
                                m.invoke(me, obj.getBoolean(key));
                            }else if(type.getName().contains("Integer")){
                                m.invoke(me, obj.getInt(key));
                            }else if(type.getName().contains("Double")){
                                m.invoke(me, obj.getDouble(key));
                            }else if(type.newInstance() instanceof DataObject){
                                // Process Nested Object
                                DataObject instance = (DataObject)type.newInstance();
                                instance.processObject(obj.getJSONObject(key), instance);

                                m.invoke(me, type.cast(instance));

                            }else if(type.newInstance() instanceof List){
                                // Process nested list of objects
                                try {
                                    ParameterizedType pType = (ParameterizedType) me.getClass().getDeclaredField(key).getGenericType();
                                    Class clazz = (Class)pType.getActualTypeArguments()[0];
                                    Log.d("******Process Object", "Type for List: " + clazz.getName());

                                    DataObject instance = (DataObject) clazz.newInstance();
                                    m.invoke(me, instance.fromJson(obj.getJSONArray(key), instance));
                                }catch(NoSuchFieldException e){
                                    Log.e("******Process Object", "Could not find class Name for " + key, e);
                                }

                            }else{
                                m.invoke(me, obj.getString(key));
                            }
                        }

                    }
    //                Log.e("****Process Object", "Set: " + me);

                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e){
            e.printStackTrace();
        }

    }



//    public abstract static DataObject fetch(int id);

//    public static ArrayList fetchAll(){
//        //RestClient client
//
//
//
//        return null;
//    }

    public void save(){
        RestClient client = RestClient.sharedClient();

        Map params = this.toMap();
        String url = this.getURL() + "/";
        if (this.id != null){
            url += this.id + "/";
        }
        this.setAuthentication(client, params);
        client.post(url, params, new RestResponseListener() {
            @Override
            public void processResponse(RCResponse response) {
                if (response.getStatus_code() == 200) {
                    saveSuccessful();
                }else{
                    Log.e(TAG, response.getContent());
                    saveFailed();
                }
            }
        });
    }

    public JSONObject toJSON(){
        return toJSON(false);
    }

    public JSONObject toJSON(boolean useMetaWrapper){
        JSONObject data = new JSONObject();
        try{
            BeanInfo info = Introspector.getBeanInfo(((Object)this).getClass());
            for(PropertyDescriptor d:info.getPropertyDescriptors()){
                Class type = d.getPropertyType();
                Method m = d.getWriteMethod();
                Object value = d.getReadMethod().invoke(this);
                Log.d("****Generate Object", "Key: " + d.getName());
                Log.e("****Generate Object", "Type: " + type);
                Log.d("****Generate Object", "Value: " + value);
                Log.d("****Generate Object", "Primitive: " + type.isPrimitive());
                if (value != null && !d.getName().equals("class")) {
                    if (!Modifier.isFinal(type.getModifiers()) && type.newInstance() instanceof DataObject){
                        Log.e("****Generate Object", "Generating for nested object****************************************");
                        data.put(d.getName(), ((DataObject)value).toJSON());
                    }else if (!Modifier.isFinal(type.getModifiers()) && type.newInstance() instanceof List){
                        Log.e("****Generate Object", "Generating for nested object****************************************");
                        JSONArray array = new JSONArray();
                        for(DataObject b: ((ArrayList<DataObject>) value)){
                           array.put(b.toJSON());
                        }
                        data.put(d.getName(), array);
                    }else {
                        Log.e("****Generate Object", "Writing value: " + value + " to " + d.getName());
                        data.put(d.getName(), value);
                    }
                }
            }
            if (useMetaWrapper && getMetaKey() != null && !getMetaKey().equals("")){
                JSONObject obj = new JSONObject();
                obj.put(getMetaKey(), data);
                data = obj;
            }
        } catch (InvocationTargetException e) {
            Log.e("****Generate Object", e.getMessage() ,e);
            e.printStackTrace();
        } catch (IntrospectionException e) {
            Log.e("****Generate Object", e.getMessage() ,e);
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            Log.e("****Generate Object", e.getMessage() ,e);
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("****Generate Object", e.getMessage() ,e);
            e.printStackTrace();
        } catch (InstantiationException e) {
            Log.e("****Generate Object", e.getMessage() ,e);
            e.printStackTrace();
        }

        return data;
    }


    public Map toMap(){
        HashMap<String, String> data = new HashMap();
        try{
        BeanInfo info = Introspector.getBeanInfo(((Object)this).getClass());
        for(PropertyDescriptor d:info.getPropertyDescriptors()){
            Object value = d.getReadMethod().invoke(this);
            if (value != null) {
                data.put(d.getName(), value.toString());
            }
        }
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return data;
    }
    protected abstract void saveSuccessful();
    protected abstract void saveFailed();

    public static DataObject fetch(Class klass, int id){
        try{
            final DataObject data =(DataObject) klass.newInstance();
        RestClient client = RestClient.sharedClient();
        Map params = new HashMap<String, String>();
        data.setAuthentication(client, params);
        client.get(data.getURL() + "/" + id + "/", params, new RestResponseListener() {
            @Override
            public void processResponse(RCResponse response) {
//                Log.e("****FETCH DETAIL", "Status Code: " + response.getStatus_code());
                if (response.getStatus_code() == 200) {
                    try {
                        Log.e("****FETCH DETAIL", "JSON Response: " + response.getContent());
                        data.fromJson(response.getContent());
                        data.fetchSuccessful();


                    } catch (Exception e) {
                        Log.e("****FETCH ERROR", "message: " + e);
                    }
                } else {
                    data.fetchFailed();
                }
            }
        });

        return data;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected abstract void fetchSuccessful();
    protected abstract void fetchFailed();

    protected abstract void setAuthentication(RestClient client, Map requestParams);
    protected abstract String getMetaKey();

    protected String getPurialMetaKey(){
        return Strings.pluralize(getMetaKey());
    }
}
