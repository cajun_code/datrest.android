package com.cajuncode.datrest.test;

import android.util.Log;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowLog;
import java.lang.Exception;
import java.lang.String;

import dalvik.annotation.TestTargetClass;

import static org.junit.Assert.*;
import com.cajuncode.datrest.models.Person;
import com.cajuncode.datrest.models.Order;
import java.util.ArrayList;
import org.json.JSONObject;

@RunWith(RobolectricTestRunner.class)
public class DataObjectTest{
    private static String TEST_TAG = "*********Test Data Object";
    @Before
    public void setUp() throws Exception {
        ShadowLog.stream = System.out;
        //you other setup here
    }
    @Test
    public void testFromJSONParsesPopulatesProperties() throws Exception{
        Log.d(TEST_TAG, "Testing fromJSON parses and populates Properties");
        String jsonString = "{\"first_name\":\"Tom\", \"last_name\":\"Petty\"}";
        Person test = new Person();
        test.fromJson(jsonString);
        assertEquals("Tom", test.getFirst_name());
        assertEquals("Petty", test.getLast_name());
    }

    @Test
    public void testFromJSONWithNestedObject() throws Exception{
        String jsonString = "{\n" +
                "\t\"first_name\" : \"Tom\",\n" +
                "\t\"last_name\"  : \"Petty\",\n" +
                "\t\"address\"    : {\n" +
                "\t\t\"street_number\": 123,\n" +
                "\t\t\"street_name\"  : \"Main Street\",\n" +
                "\t\t\"city\"         : \"Atlanta\", \n" +
                "\t\t\"state\"        : \"GA\",\n" +
                "\t\t\"zip\" \t\t  : \"12345\"\n" +
                "\t}\n" +
                "} ";
        Person test = new Person();
        test.fromJson(jsonString);
        assertEquals("Tom", test.getFirst_name());
        assertEquals("Petty", test.getLast_name());
        assertNotNull(test.getAddress());
        assertNotEquals(test.getAddress(), "{\"zip\":\"12345\",\"street_name\":\"Main Street\",\"state\":\"GA\",\"street_number\":123,\"city\":\"Atlanta\"}")  ;

        assertEquals("Main Street", test.getAddress().getStreet_name());
    }

    @Test
    public void testFromJsonWithNestedList() throws Exception{
        String jsonString = "{\n" +
                "\t\"id\" : 1,\n" +
                "\t\"customer\" : {\n" +
                "\t\t\"first_name\": \"Tom\",\n" +
                "\t\t\"last_name\" : \"Petty\",\n" +
                "\t},\n" +
                "\t\"shipping_address\":{\n" +
                "\t\t\"zip\":\"12345\",\n" +
                "\t\t\"street_name\":\"Main Street\",\n" +
                "\t\t\"state\":\"GA\",\n" +
                "\t\t\"street_number\":123,\n" +
                "\t\t\"city\":\"Atlanta\"\n" +
                "\t},\n" +
                "\t\"billing_address\":{\n" +
                "\t\t\"zip\":\"12345\",\n" +
                "\t\t\"street_name\":\"Main Street\",\n" +
                "\t\t\"state\":\"GA\",\n" +
                "\t\t\"street_number\":123,\n" +
                "\t\t\"city\":\"Atlanta\"\n" +
                "\t},\n" +
                "\t\"items\": [\n" +
                "\t\t{\"item\": {\"sku\": \"123456-335\", \"name\": \"dog\", \"price\": 100.25}, \"quantity\": 1, \"extended_price\": 100.25},\n" +
                "\t\t{\"item\": {\"sku\": \"123456-778\", \"name\": \"sheep\", \"price\": 700}, \"quantity\": 15, \"extended_price\": 10500},\n" +
                "\t\t{\"item\": {\"sku\": \"123456-111\", \"name\": \"sheer\", \"price\": 50}, \"quantity\": 5, \"extended_price\": 250},\n" +
                "\t]\n" +
                "\t\n" +
                "}";
        Order order = new Order();
        order.fromJson(jsonString);
        assertEquals(order.getItems().size(), 3);

    }

    @Test
    public void testFromJsonForArrayWithMetaObject() throws Exception{
        String jsonString = "{\"people\": [{\"first_name\":\"Tom\", \"last_name\":\"Petty\"}," +
                "{\"first_name\":\"Richard\", \"last_name\":\"Petty\"}, " +
                "{\"first_name\":\"Kyle\", \"last_name\":\"Petty\"}, ]}";
        Person test = new Person();
        ArrayList<Person> people = test.fromJson(jsonString);
        test = people.get(0);
        assertEquals("Tom", test.getFirst_name());
        assertEquals("Petty", test.getLast_name());
        assertEquals(people.size(), 3);
    }

    @Test
    public void testFromJsonWithMetaObject() throws Exception{
        String jsonString = "{\"person\": {\"first_name\":\"Tom\", \"last_name\":\"Petty\"}" +
                "}";
        Person test = new Person();
        //ArrayList<Person> people = test.fromJson(jsonString);
        test.fromJson(jsonString);
        assertEquals("Tom", test.getFirst_name());
        assertEquals("Petty", test.getLast_name());

    }

    @Test
    public void testToJsonWithMetaObject() throws Exception{
        String jsonString = "{\"person\":{\"first_name\":\"Tom\",\"last_name\":\"Petty\"}" +
                "}";
        Person test = new Person();
        //ArrayList<Person> people = test.fromJson(jsonString);
        test.fromJson(jsonString);
        assertEquals(jsonString, test.toJSON(true).toString());


    }

    @Test
    public void testToJSONWithNestedObject() throws Exception{
        String jsonString = "{\"person\":{\n" +
                "\t\"first_name\" : \"Tom\",\n" +
                "\t\"last_name\"  : \"Petty\",\n" +
                "\t\"address\"    : {\n" +
                "\t\t\"street_number\": 123,\n" +
                "\t\t\"street_name\"  : \"Main Street\",\n" +
                "\t\t\"city\"         : \"Atlanta\", \n" +
                "\t\t\"state\"        : \"GA\",\n" +
                "\t\t\"zip\" \t\t  : \"12345\"\n" +
                "\t}\n" +
                "}} ";

        Person test = new Person();
        test.fromJson(jsonString);
        assertEquals(new JSONObject(jsonString).toString(), test.toJSON(true).toString());
    }

    @Test
    public void testToJsonWithNestedList() throws Exception{
        Log.d(TEST_TAG, "Testing to JSON with Nested LIst");
        String jsonString = "{\n" +
                "\t\"id\" : \"1\",\n" +
                "\t\"customer\" : {\n" +
                "\t\t\"first_name\": \"Tom\",\n" +
                "\t\t\"last_name\" : \"Petty\",\n" +
                "\t},\n" +
                "\t\"shipping_address\":{\n" +
                "\t\t\"zip\":\"12345\",\n" +
                "\t\t\"street_name\":\"Main Street\",\n" +
                "\t\t\"state\":\"GA\",\n" +
                "\t\t\"street_number\":123,\n" +
                "\t\t\"city\":\"Atlanta\"\n" +
                "\t},\n" +
                "\t\"billing_address\":{\n" +
                "\t\t\"zip\":\"12345\",\n" +
                "\t\t\"street_name\":\"Main Street\",\n" +
                "\t\t\"state\":\"GA\",\n" +
                "\t\t\"street_number\":123,\n" +
                "\t\t\"city\":\"Atlanta\"\n" +
                "\t},\n" +
                "\t\"items\": [\n" +
                "\t\t{\"item\": {\"sku\": \"123456-335\", \"name\": \"dog\", \"price\": 100.25}, \"quantity\": 1, \"extended_price\": 100.25},\n" +
                "\t\t{\"item\": {\"sku\": \"123456-778\", \"name\": \"sheep\", \"price\": 700}, \"quantity\": 15, \"extended_price\": 10500},\n" +
                "\t\t{\"item\": {\"sku\": \"123456-111\", \"name\": \"sheer\", \"price\": 50}, \"quantity\": 5, \"extended_price\": 250},\n" +
                "\t]\n" +
                "\t\n" +
                "}";
        Order order = new Order();
        order.fromJson(jsonString);
        JSONObject exptected = new JSONObject(jsonString);
        JSONObject processed = order.toJSON();
        Log.d(TEST_TAG, "Exp: " + exptected.toString());
        Log.d(TEST_TAG, "Pro: " + processed.toString());
        assertEquals(exptected.toString(), processed.toString());

    }
}
