package com.cajuncode.datrest.models;

import java.lang.Double;
import java.lang.Integer;
import java.lang.String;

public class OrderItems extends TestDataObject{

    private Product item;
    private Integer quantity;
    private Double extended_price;

    public Product getItem() {
        return item;
    }

    public void setItem(Product item) {
        this.item = item;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getExtended_price() {
        return extended_price;
    }

    public void setExtended_price(Double extended_price) {
        this.extended_price = extended_price;
    }
}