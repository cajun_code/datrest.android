package com.cajuncode.datrest.models;

import com.cajuncode.datrest.models.DataObject;
import com.cajuncode.datrest.models.DataObject;
import com.cajuncode.restclient.RestClient;

import java.lang.Override;
import java.lang.String;
import java.util.Map;

public abstract class TestDataObject extends DataObject{
    @Override
    protected void setAuthentication(RestClient client, Map requestParams) {

    }

    @Override
    protected void saveSuccessful() {

    }

    @Override
    protected void saveFailed() {

    }

    @Override
    protected void fetchSuccessful() {

    }

    @Override
    protected void fetchFailed() {

    }

    @Override
    public String getURL() {
        return null;
    }

    @Override
    protected String getMetaKey() {
        return "";
    }
}